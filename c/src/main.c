#include "factorial.h"

int main() {
    int number = 5;
    long result = factorial(number);
    printf("The factorial of %d is %ld!\n", number, result);
    return 0;
}