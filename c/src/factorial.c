#include "factorial.h"
#include <CoreFoundation/CoreFoundation.h>

long factorial(int n) {
    // do some logging with the help of CoreFoundation
    CFShow(CFStringCreateWithFormat(NULL, NULL, CFSTR("factorial(%d)"), n));

    if (n == 0 || n == 1) {
        return 1;
    }
    return n * factorial(n-1);
}