# Deep dive into Swift frameworks

Use the makefiles to build the examples:

* C - a static or dynamic library linked to a C app
* Swift - how to use the swiftc command
* Xcode - building an universal fat framework & using it


### License

[WTFPL](LICENSE)
