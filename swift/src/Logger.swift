public func log(_ message: String) {
    print(fullLogString(message))
}

internal func fullLogString(_ message: String) -> String {
    return "(Logger): \(message)"
}