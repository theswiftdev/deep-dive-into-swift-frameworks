import Logger

public func greet(_ name: String) {
    log("Hello \(name)!")
}