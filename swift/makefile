BUILD_DIR = .build
SRC_DIR = ./src

# this method is based on the following gist:
# https://gist.github.com/briancroom/5d0f1b966fa9ef0ae4950e97f9d76f77

all: clean prepare run-main

prepare:
	mkdir -p $(BUILD_DIR)

#building a static lib
build-logger:
	cd $(BUILD_DIR)
	swiftc -emit-object -emit-module -force-single-frontend-invocation -parse-as-library $(SRC_DIR)/Logger.swift
	mkdir -p $(BUILD_DIR)/Logger
	mv Logger.o $(BUILD_DIR)/Logger
	mv Logger.swiftdoc $(BUILD_DIR)/Logger
	mv Logger.swiftmodule $(BUILD_DIR)/Logger
	ar -r $(BUILD_DIR)/Logger/libLogger.a $(BUILD_DIR)/Logger/Logger.o

#building a dynamic lib
build-greet: build-logger
	swiftc -emit-library -emit-module -parse-as-library \
	-I$(BUILD_DIR)/Logger -lLogger -Xlinker \
	-L$(BUILD_DIR)/Logger -Xlinker -install_name \
	-Xlinker @rpath/libGreeter.dylib \
	$(SRC_DIR)/Greeter.swift
	mkdir -p $(BUILD_DIR)/Greeter
	mv libGreeter.dylib $(BUILD_DIR)/Greeter
	mv Greeter.swiftdoc $(BUILD_DIR)/Greeter
	mv Greeter.swiftmodule $(BUILD_DIR)/Greeter


build-main: build-greet
	swiftc -emit-executable \
	-I$(BUILD_DIR)/Greeter \
	-I$(BUILD_DIR)/Logger \
	-lGreeter -Xlinker \
	-L$(BUILD_DIR)/Greeter \
	-Xlinker -rpath -Xlinker $(BUILD_DIR)/Greeter \
	$(SRC_DIR)/main.swift
	mv main $(BUILD_DIR)/app

run-main: build-main
	$(BUILD_DIR)/app

clean:
	rm -rf $(BUILD_DIR)